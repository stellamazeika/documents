Members
===
| Name | Pronouns | Twitch URL | Twitter Handle |
| ---- | -------- | ---------- | -------------- |
| amethyst_rocks | she/her | https://twitch.tv/amethyst_rocks | https://twitter.com/ame_rocks |
| garbitheglitcheress | she/her | https://twitch.tv/garbitheglitcheress | https://twitter.com/GarbiGlitchress |
| leggystarscream | she/her | https://twitch.tv/leggystarscream | https://twitter.com/leggystarscream |
| megMacAttack | she/her or they/them | https://twitch.tv/megmacattack | https://twitter.com/_megmac_ |
| mustbetuesday | they/them | https://twitch.tv/mustbetuesdaymusic | https://twitter.com/mustbetuesday |
| rubash319 | she/her or they/them | https://twitch.tv/rubash319 | https://twitter.com/rubash319 |
| seeliefae | fae/faer | https://twitch.tv/seeliefae | https://twitter.com/seeliefaetv |
| zoevermilion | she/her | https://twitch.tv/zoevermilion | https://twitter.com/zoevermilion |