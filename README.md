Edge Case Collective Statement of Purpose
===
This document describes the founding principles of a group of streamers and creatives that look at things from a queer perspective. Our goal is to build each other up, create interesting and entertaining work together, and to try to do so from the perspective of queer people who play queer games.

The goal here is for this to not merely be a label on our streams that signals some shared interest, but to be a group of people who are committed to building something together and in so doing improve our own streams and creative works. To use cliche, our hope is that the whole is greater than the sum of its parts, and that a rising tide will lift all boats.

Queering Games
---
Queering is the act of taking something within normative structures (typically heteronormativity, cisnormativity, etc) and reexamining it from a lense that deconstructs the dominant narrative and builds something new and different and challenging in its place.

Taking a game and turning it upside down with glitches, or romhacks, or just generally playing it in new and unexpected ways as is common in speedrunning, is arguably an act of queering. It is on that argument that this core idea rests. Our existence as (primarily) queer people, playing queer games, is a challenge to normative ideas about games and their place in the world.

It can also be difficult to find traction on a platform like twitch as a small unknown, playing weird things, looking at things from a perspective that challenges homophobia, transphobia, ableism, and other issues like that. It's frequently the case that we feel that in order to get along in this world we have to abandon principles to get along. Hopefully this group can provide solidarity there as well.

Obviously, though, all of this should be in the name of producing fun content. The goal is not to be overtly political necessarily, but that this is simply something that we are and believe in and enjoy doing. It's a lense to focus efforts through.

Goals and Expectations
---
The fundamental goals of a stream team should be to build up a shared audience, and to build up the members and give them better opportunities for growth than they'd have working alone. Our efforts should be directed where possible towards helping each other.

To that end, at the very most basic level, every member of the group should consider the other members of the group people that they would be willing to host after a good stream (if they are streaming at the time). They should also, hopefully, consider the other members to be people that they would be willing to do collaborations with (races, cooperative runs, community games like jackbox maybe, creative endeavours to produce new content for each other, etc).

Ideally if we can work together to have stream schedules sometimes coincide in such a way that we can be passing viewers off to each other when appropriate, that would be fantastic. Just a simple effort to be aware of each others' plans and work them into our own where we can, basically.

Some of our members may not be terribly active streamers, but may be providing other helpful efforts to other teammembers in creative ways. Layouts, music, hacks, and other interesting content that can be used on stream and fits our shared ideals are all valid and meaningful contributions to the team. It takes all sorts to create interesting and entertaining content.

We should have space where we can talk about plans from small to grand ideas and have them workshopped and discussed and draw on the resources of the group to make them better.

We should have regular meetings (monthly?) to discuss plans and build cohesion as a group.

Concrete Plans
---
This will need more discussion, but things we could maybe work on include:

- regular community game nights of some sort (randos, jackbox-ish stuff?)
- a podcast?
- romhacks?
- marathons/showcases (as fundraisers both for charity and for travel costs?)
- ???

Things We Need
---
Also up for discussion.

- Discord bots
- Twitter account (pre-emptively made @edgecaseclctve -- this shortening may be an argument against the name)
- Twitch account (pre-emptively made edgecasecollective)
- ???